from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
  
import numpy
  
ext = Extension("enipy.lxcTools", ["enipy/lxcTools.pyx"], 
    include_dirs = [numpy.get_include()])
  
setup( 
	name='enipy', 
	version='0.2dev',
	packages=['enipy',],
	ext_modules=[ext],
	license=open('LICENSE.txt').read(),
	long_description=open('README.txt').read(),
	cmdclass = {'build_ext': build_ext}	
	)
